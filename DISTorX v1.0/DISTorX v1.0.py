from __future__ import division
from pylab import *
from numpy import *
from Tkinter import *
import tkFileDialog
from visual import *
from visual.controls import *
from matplotlib import *
from matplotlib import style
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import math
import cmath
from time import sleep
from decimal import *

global FilePath
global ExpFilePath
global CompareFilePath
CompareFilePath = '-EMPTY-'
global dt
global time
time = float(0.0)

class App:    
    def __init__(self, master):

        global dt
                
        frame = Frame(master)
        frame.grid()

        frame.master.title('DISTorX V 1.0')
        
        self.label = Label(frame, text = "\n\t\tProgram DISTorX v1.0 \t\t\n")
        self.label.grid(row = 0, column = 0)

        self.label = Label(frame, text = "\t Visualization of distorted structure: \t\t")
        self.label.grid(row = 2, column = 0, sticky=W)

        self.vizual = Button(frame, borderwidth = 4, width = 20, text = "Visualization", command=self.viz)
        self.vizual.grid(row = 2, column = 1)
        
        self.label = Label(frame, text = "\t Symulation of distorted structure: \t\t")
        self.label.grid(row = 3, column = 0, sticky=W)
        
        self.simul = Button(frame, borderwidth = 4, width = 20, text = "Simulation", command=self.sim1)
        self.simul.grid(row = 3, column = 1)

        self.label = Label(frame, text = "\t Symulation of X ray pattern for input File: \t\t")
        self.label.grid(row = 4, column = 0, sticky=W)

        self.simul = Button(frame, borderwidth = 4, width = 20, text = "Simulation X", command=self.simX)
        self.simul.grid(row = 4, column = 1)
        
        self.label = Label(frame, text = "\t Dynamic Plot: \t\t")
        self.label.grid(row = 6, column = 0, sticky=W)

        self.dynam = Button(frame, borderwidth = 4, width = 20, text = "Dynamic", command=self.dyna)
        self.dynam.grid(row = 6, column = 1)

        self.label = Label(frame, text = "\t Symulation of X ray pattern for Dynamic Plot: \t\t")
        self.label.grid(row = 5, column = 0, sticky=W)

        self.simulx = Button(frame, borderwidth = 4, width = 20, text = "Simulation Dynamic X", command=self.simXdyn)
        self.simulx.grid(row = 5, column = 1)

        self.label = Label(frame, text = "\n Kalinowski 2016 \n ")
        self.label.grid(row = 9, column = 0)

        self.button1 = Button(frame, borderwidth = 4, width = 20, text = "  EXIT  ", command=self.close)
        self.button1.grid(row = 9, column = 1)

        self.label = Label(frame, text = "\t Show and compare distortion with experiment:")
        self.label.grid(row = 11, column = 0, sticky=W)

        self.label = Label(frame, text = "\t")
        self.label.grid(row = 13, column = 0, sticky=W)

        self.button3 = Button(frame, borderwidth = 4, width = 20, text="Show Graph", command=self.expGraph)
        self.button3.grid (row = 11, column = 1)

        self.label = Label(frame, text = "\t Set zero for experimental data: ")
        self.label.grid(row = 11, column = 2, sticky=W)

        self.setzero = Button(frame, borderwidth = 4, width = 20, text="Set Zero", command = self.setzero)
        self.setzero.grid (row = 11, column = 3)

        self.label = Label(frame, text = "\t Intensity match with experimental data: ")
        self.label.grid(row = 12, column = 2, sticky=W)

        self.intenmatch = Button(frame, borderwidth = 4, width = 20, text= "Intensity match", command = self.intenMatch)
        self.intenmatch.grid (row = 12, column = 3)
        
        self.label = Label(frame, text = "\t Compound Name: ")
        self.label.grid(row = 0, column = 2, sticky = E)

        self.label = Label(frame, text = "\t\t")
        self.label.grid(row = 0, column = 4, sticky = E)
        
        self.name = StringVar()
        self.myentry = Entry(frame, width = 20, textvariable=self.name)
        self.myentry.grid(row=0,column=3 )
        self.name.set('Ce3Ru4Sn13')
# input  atoms
        self.label = Label(frame, text = "Get line nr. from AFF.txt ")
        self.label.grid(row = 0, column = 5, sticky = W)
        
        self.label = Label(frame, text = "Atom 1")
        self.label.grid(row = 1, column = 5, sticky = W)
        
        self.x1 = IntVar()
        self.myentry = Entry(frame, width = 10, textvariable=self.x1)
        self.myentry.grid(row=2,column=5, sticky = W )
        self.x1.set(124)

        self.label = Label(frame, text = "Atom 2")
        self.label.grid(row = 3, column = 5, sticky = W)
        
        self.x2 = IntVar()
        self.myentry = Entry(frame, width = 10, textvariable=self.x2)
        self.myentry.grid(row=4,column=5, sticky = W )
        self.x2.set(93)

        self.label = Label(frame, text = "Atom 3")
        self.label.grid(row = 5, column = 5, sticky = W)
        
        self.x3 = IntVar()
        self.myentry = Entry(frame, width = 10, textvariable=self.x3)
        self.myentry.grid(row=6,column=5, sticky = W )
        self.x3.set(108)

# end of input  atoms

        self.label = Label(frame, text = "\t Maximum time parameter: ")
        self.label.grid(row = 1, column = 2, sticky = E)
        
        self.maxtime = DoubleVar()
        self.myentry = Entry(frame, width = 10, textvariable=self.maxtime)
        self.myentry.grid(row=1,column=3 )
        self.maxtime.set(float(10))

        self.label = Label(frame, text = "\t Time interval (dt): ")
        self.label.grid(row = 2, column = 2, sticky = E)
     
        self.dt = DoubleVar()
        self.myentry = Entry(frame, width = 10, textvariable=self.dt)
        self.myentry.grid(row=2,column=3 )
        self.dt.set(float(0.1))
                
        self.label = Label(frame, text = "\t Ampitude of distrotion wave: ")
        self.label.grid(row = 3, column = 2, sticky = E)
        
        self.A = DoubleVar()
        self.myentry = Entry(frame, width = 10, textvariable=self.A)
        self.myentry.grid(row=3,column=3 )
        self.A.set(float(0.05))
        
#STRUCTURE PARAMETERS
        
        self.label = Label(frame, text = "\t Wavelenght " + (u'\u03bb') + ":")
        self.label.grid(row = 4, column = 2, sticky = E)
        
        self.lamb = DoubleVar()
        self.myentry = Entry(frame, width = 10, textvariable=self.lamb)
        self.myentry.grid(row=4,column=3 )
        self.lamb.set(float(1.54056))
        

        self.label = Label(frame, text = "\t Lattice constant, a: ")
        self.label.grid(row = 5, column = 2, sticky = E)
        
        self.a = DoubleVar()
        self.myentry = Entry(frame, width = 10, textvariable=self.a)
        self.myentry.grid(row=5,column=3 )
        self.a.set(float(9.72819))
        
        self.label = Label(frame, text = "\t Lattice constant, b: ")
        self.label.grid(row = 6, column = 2, sticky = E)
        
        self.b = DoubleVar()
        self.myentry = Entry(frame, width = 10, textvariable=self.b)
        self.myentry.grid(row=6,column=3 )
        self.b.set(float(9.72819))
        
        self.label = Label(frame, text = "\t Lattice constant, c: ")
        self.label.grid(row = 7, column = 2, sticky = E)
        
        self.c = DoubleVar()
        self.myentry = Entry(frame, width = 10, textvariable=self.c)
        self.myentry.grid(row=7,column=3 )
        self.c.set(float(19.45638))
        
#ball radius
        
        self.label = Label(frame, text = "\t Ball Radius: ")
        self.label.grid(row = 8, column = 2, sticky = E)
        
        self.ballradius = DoubleVar()
        self.myentry = Entry(frame, width = 10, textvariable=self.ballradius)
        self.myentry.grid(row=8,column=3 )
        self.ballradius.set(float(0.3))

#input files loading

        self.label = Label(frame, text = "\t Set input File *.xyz: \t\t")
        self.label.grid(row = 1, column = 0, sticky=W)
        
        self.File = Button(frame, borderwidth = 4, width = 20, text = "Open File", command=self.Files)
        self.File.grid(row = 1, column = 1)

        self.label = Label(frame, text = "\t Set input Experiment File *.xy: \t\t")
        self.label.grid(row = 12, column = 0, sticky=W)
        
        self.File = Button(frame, borderwidth = 4, width = 20, text = "Open Experiment File", command=self.OpenExp)
        self.File.grid(row = 12, column = 1)

        self.label = Label(frame, text = "\t Set input Comparision File *.xy: \t\t")
        self.label.grid(row = 13, column = 0, sticky=W)

        self.comFile = Button(frame, borderwidth = 4, width = 20, text = "Open Comparision File", command=self.OpenCompFile)
        self.comFile.grid(row = 13, column = 1)

        self.labelDyn = Label(frame, text = "\t")
        self.labelDyn.grid(row = 13, column = 3)

        self.labelDyn = Label(frame, text = "\t Next or Previous Comparision File: ")
        self.labelDyn.grid(row = 13, column = 2, sticky = W)

        self.comFileDynPrevious = Button(frame, borderwidth = 4, width = 8, text = "<", command = self.PreviousFile)
        self.comFileDynPrevious.grid(row = 13, column = 3, sticky = W)

        global CompareFilePath
        
        self.CoPath = Label(frame, width = 70, text=CompareFilePath)
        self.CoPath.grid(row=15,column=0, sticky=W)
        
        self.comFileDynNext = Button(frame, borderwidth = 4, width = 8, text = ">", command=self.NextFile)
        self.comFileDynNext.grid(row = 13, column = 3, sticky = E)

        self.label = Label(frame, text = "\t")
        self.label.grid(row = 16, column = 0, sticky=W)              

    def Files(self):
        global FilePath
        self.fileName = tkFileDialog.askopenfilename( filetypes = ( ("input_file", "*.xyz"),("All files", "*.*")))
        FilePath = self.fileName

    def OpenExp(self):
        global ExpFilePath
        self.fileName = tkFileDialog.askopenfilename( filetypes = ( ("experimental_input_file", "*.xy"),("All files", "*.*")))
        ExpFilePath = self.fileName
        
    def OpenCompFile(self):
        global CompareFilePath
        self.fileName = tkFileDialog.askopenfilename( filetypes = ( ("to_compare_input_file", "*.xy"),("All files", "*.*")))
        CompareFilePath = self.fileName
        self.CoPath.config(text=CompareFilePath)
        
    def NextFile(self):
        global CompareFilePath
        global time
              
        dt = self.dt.get()
        
        CompareFilePath = ("SYMx_Norm\WIDMO_" + str(time) + "_time_Normalized.xy")
        time = time + dt        
        print CompareFilePath
        return time
    
    def PreviousFile(self):
        global CompareFilePath
        global time
               
        dt = self.dt.get()
               
        CompareFilePath = ("SYMx_Norm\WIDMO_" + str(time) + "_time_Normalized.xy")
        time = time - dt       
        print CompareFilePath
        return time

    def PrintVar(x):
        print x
              
###############################################################################
#VISUALIZATION PROGRAM ########################################################
###############################################################################
      
    def viz(self):
        
        print FilePath
        
        f=open(FilePath,'r+')
        i=1
        At1 = 0
        At2 = 0
        At3 = 0
        N = 0
        sym = []
        X = []
        Y = []
        Z = []
        Znew = []
        ball = []
     
        scene = display(title='Visualisation of distorted structure', x=0, y=0, width=800, range = (20,20,20), height=600, background=(1,1,1))
        
        scene.fullscreen = False
        scene.autocenter = True
        scene.exit = True
        
        for line in f:
            [num,x,y,z]=line.split()
            if num=='Ce':
                cl=color.green
                At1 = At1 + 1
            if num=='Ru':
                cl=color.red
                At2 = At2 + 1
            if num=='Sn':
                cl=color.blue
                At3 = At3 + 1
            N = N+1

            ball.append(sphere(pos=( float(x),float(y),float(z)), radius=self.ballradius.get(), color=cl ))
 
            sym.append(str(num))
            X.append(float(x))
            Y.append(float(y))
            Z.append(float(z))

        dt = self.dt.get()
        time = 0
        maxtime = self.maxtime.get()

        while time < maxtime:
            rate(50)
                     
            time = time + dt
            A = self.A.get()

            print time

            for i in range (0, N):
                
                if sym[i]=='Ru':

                    Znew = Z[i] + A*cos(2*math.pi*Z[i]+time)
                    ball[i].pos = (X[i],Y[i],Znew)
            
                if sym[i]=='Sn':

                    Znew = Z[i] + A*cos(2*math.pi*Z[i]+time)
                    ball[i].pos = (X[i],Y[i],Znew)

        close()
    
##################################################################################            
# SIMULATION GEN. of XYZ OUT ####################################################
##################################################################################

    def sim1(self):
        f=open(FilePath,'r+')
        
        sym = []
        X = []
        Y = []
        Z = []
        Znew = []
        Zout = [0.0,0.0,0.0]
        N=0  
                             
        for line in f:
            [num,x,y,z]=line.split()
                      
            sym.append(str(num))
            X.append(float(x))
            Y.append(float(y))
            Z.append(float(z))
            N = N + 1

        dt = self.dt.get()
        time = 0
        maxtime = self.maxtime.get()
        i=0
        
        while time < maxtime:
            rate (1000)

            time = time + dt
            A = self.A.get()
            f1=open("SYM/output_file" + str(time) + "_time.xyz",'w+')

            print time

            for i in range (0, N):
                
                if sym[i]=='Ru':

                    Znew = Z[i] + A*cos(2*math.pi*Z[i]) #+time
                    Zout = (X[i],Y[i],Znew)
            
                if sym[i]=='Sn':

                    Znew = Z[i] + A*cos(2*math.pi*Z[i]) #+time
                    Zout = (X[i],Y[i],Znew)
                        
                f1.write(str(sym[i]))
                f1.write(" ")
                f1.write(str(Zout[0]))
                f1.write(" ")
                f1.write(str(Zout[1]))
                f1.write(" ")
                f1.write(str(Zout[2]))
                f1.write("\n")
            f1.close()


#############################################################################################
#DYNAMIC PLOT################################################################################
#############################################################################################

    def dyna(sefl):
                                          
        #print dt
        #dt = self.dt.get()
        #global maxtime
        #maxtime = self.maxtime.get()
        #time = 0
        
        fig = plt.figure()
        p = fig.add_subplot(1,1,1)

        global time
        time = 0.1
        
        
        def animate(self):
            #dt = self.dt.get()
            global time
                        
            xs = []
            ys = []
                        
            graph_data = open("SYMx\WIDMO_" + str(time) + "_time.xy", 'r+').read()

            print "SYMx\WIDMO_" + str(time) + "_time.xy"
            #graph_data = open("WIDMO.txt", 'r+').read()
            lines = graph_data.split('\n')
    
            width = 0.1       
            
            for line in lines:
                if len(line)>1:
                    x, y = line.split(' ')
                    xs.append(float(x))
                    ys.append(float(y))
                                
            p = plt.bar(xs, ys, width)
            
            plt.ylabel('Intensity')
            plt.xlabel('2 theta')
            plt.title('Simulated X-Ray Pattern')
            
            dt = 0.1
            time=time+dt
            del xs
            del ys
            
        ani = animation.FuncAnimation(fig, animate, interval=5000) 
        plt.show()
        
###################################################################################
#SIMULATION XRAY PATTERN ###########################################################
###################################################################################

    def simX(self):
                
        f=open(FilePath,'r+')
        At1 = 0
        At2 = 0
        At3 = 0
        N = 0
        for line in f:
            [num,x,y,z]=line.split()
            if num=='Ce':
                At1 = At1 + 1
            if num=='Ru':
                At2 = At2 + 1
            if num=='Sn':
                At3 = At3 + 1
            N = N + 1            
        f.close()
            
        At1At2 = At1 + At2
        At1At2At3 = At1 + At2 + At3
        
        print At1At2At3

        lamb = self.lamb.get()

        a = array([self.a.get(),0.0,0.0])
        b = array([0.0,self.b.get(),0.0])
        c = array([0.0,0.0,self.c.get()])
        
        V = sum(a*cross(b,c)) 

        k1 = 2*pi*(cross(b,c))/V 
        k2 = 2*pi*(cross(c,a))/V 
        k3 = 2*pi*(cross(a,b))/V

        (h ,k, l) = (0, 0, 0)

        kvec_list = []
        sin = 0

        while sin <= 1:
             kvec_list.append((h, k, l))
             p = h * k1
             q = k * k2
             r = l * k3
             L = p + q + r
             
             K = (L[0]**2 + L[1]**2 + L[2]**2)**(0.5)
             sin = lamb*(K/(4*math.pi))
             h = h + 1
             
        n = len(kvec_list)
        sin = 0
        for i in range(n):
             (h, k, l) = kvec_list[i]
             sin = 0
             while sin <= 1:
                  kvec_list.append((h, k, l))
                  p = h * k1
                  q = k * k2
                  r = l * k3
                  L = p + q + r
                  K = (L[0]**2 + L[1]**2 + L[2]**2)**(0.5)
                  sin = lamb*(K/(4*math.pi))
                  k = k + 1
          
        del kvec_list[0:n]

        n = len(kvec_list)
        sin = 0
        for i in range(n):
             (h, k, l) = kvec_list[i]
             sin = 0
             while sin <= 1:
                  kvec_list.append((h, k, l))
                  p = h * k1
                  q = k * k2
                  r = l * k3
                  L = p + q + r
                  K = (L[0]**2 + L[1]**2 + L[2]**2)**(0.5)
                  sin = lamb*(K/(4*math.pi))
                  l = l + 1                  
        del kvec_list[0:n+1]

        n = len(kvec_list)
        print n

# Aff abd STRUCTURE FACTORS

        position = genfromtxt(FilePath) 
        form = genfromtxt("AFF/AFF.txt")   

        x1 = self.x1.get()
        x2 = self.x2.get()
        x3 = self.x3.get()
        
        print x1, x2, x3

        L1 = form[x1, 1:]
        L2 = form[x2, 1:]
        L3 = form[x3, 1:]

        inten_profile = []
        inten_profile0 = []
        inten_list = []
        theta_list = []
        sFactor_list = []
        dspace_list = []

        for i in range(n):
             (h, k, l) = kvec_list[i]
             p = h * k1
             q = k * k2
             r = l * k3
             L = p + q + r
             K = (L[0]**2 + L[1]**2 + L[2]**2)**(0.5)
     
             y = K/(4*math.pi) 
             
             f1 = L1[0] * exp(-L1[1]*y**2) + L1[2] * exp(-L1[3]*y**2) + L1[4] * exp(-L1[5]*y**2) + L1[6] * exp(-L1[7]*y**2) + L1[8]
             f2 = L2[0] * exp(-L2[1]*y**2) + L2[2] * exp(-L2[3]*y**2) + L2[4] * exp(-L2[5]*y**2) + L2[6] * exp(-L2[7]*y**2) + L2[8]
             f3 = L3[0] * exp(-L3[1]*y**2) + L3[2] * exp(-L3[3]*y**2) + L3[4] * exp(-L3[5]*y**2) + L3[6] * exp(-L3[7]*y**2) + L3[8]
             
             for m in range(0,At1):
                  P = position[m,:]
                  Q = [h, k, l]

                  sFactor = f1*exp(2*math.pi*(P[1]*Q[0] + P[2]*Q[1] + P[3]*Q[2])*1j)
                  sFactor_list.append(sFactor)
                  m = m + 1
                  
             for t in range(At1, At1At2):
                  P = position[t,:]
                  Q = [h, k, l]

                  sFactor = f2*exp(2*math.pi*(P[1]*Q[0] + P[2]*Q[1] + P[3]*Q[2])*1j)
                  sFactor_list.append(sFactor)
                  t = t + 1

             for u in range(At1At2, At1At2At3):
                  P = position[u,:]
                  Q = [h, k, l]

                  sFactor = f3*exp(2*math.pi*(P[1]*Q[0] + P[2]*Q[1] + P[3]*Q[2])*1j)
                  sFactor_list.append(sFactor)
                  u = u + 1
                  
             inten = abs(sum(sFactor_list))
             inten_list.append(inten)
     
             zas = lamb*(y) 
             
             if zas < 1:

                 theta0 = math.asin(zas)
                 theta = theta0 * (2*180/math.pi)
                         
             theta_list.append(theta)

             inten_profile0.append((h, k, l, theta, inten, inten**2))
             inten_profile.append((theta, inten**2))
             del sFactor_list[:]
             i = i + 1

        A = array(inten_profile)
        print A
        savetxt('SYMx\WIDMO_0_time.xy', A)
        B = array(inten_profile0)
        savetxt('SYMx0\WIDMO_0.xy', B)

        clf()
        X, Y = B[:,3], B[:,5]
        num = len(inten_list)
        for i in range(num):
            plot([B[i,3],B[i,3]], [0,B[i,5]])
        xlabel("2$\\theta$")
        ylabel("Structure Factor$^2$")
        title(self.name.get())
        show()


#####################################################################
#SIMULATION DYNAMIC XRAY PATTERN    #################################
#####################################################################
        
    def simXdyn(self):
        dt = self.dt.get()
        maxtime = self.maxtime.get()
        time = 0
                
        while time < maxtime:
            time = time + dt
            f=open(FilePath,'r+')
            At1 = 0
            At2 = 0
            At3 = 0
            N = 0
            for line in f:
                [num,x,y,z]=line.split()
                if num=='Ce':
                    At1 = At1 + 1
                if num=='Ru':
                    At2 = At2 + 1
                if num=='Sn':
                    At3 = At3 + 1
                N = N + 1            
            f.close()
            
            At1At2 = At1 + At2
            At1At2At3 = At1 + At2 + At3
        
            print At1At2At3
            
            lamb = self.lamb.get()

            a = array([self.a.get(),0.0,0.0])
            b = array([0.0,self.b.get(),0.0])
            c = array([0.0,0.0,self.c.get()])

            V = sum(a*cross(b,c)) 

            k1 = 2*pi*(cross(b,c))/V 
            k2 = 2*pi*(cross(c,a))/V 
            k3 = 2*pi*(cross(a,b))/V

            (h ,k, l) = (0, 0, 0)

            kvec_list = []
            sin = 0

            while sin <= 1:
                 kvec_list.append((h, k, l))
                 p = h * k1
                 q = k * k2
                 r = l * k3
                 L = p + q + r
                 K = (L[0]**2 + L[1]**2 + L[2]**2)**(0.5)
                 sin = lamb*(K/(4*math.pi))
                 h = h + 1
     
            n = len(kvec_list)
            sin = 0
            for i in range(n):
                 (h, k, l) = kvec_list[i]
                 sin = 0
                 while sin <= 1:
                      kvec_list.append((h, k, l))
                      p = h * k1
                      q = k * k2
                      r = l * k3
                      L = p + q + r
                      K = (L[0]**2 + L[1]**2 + L[2]**2)**(0.5)
                      sin = lamb*(K/(4*math.pi))
                      k = k + 1
          
            del kvec_list[0:n]

            n = len(kvec_list)
            sin = 0
            for i in range(n):
                 (h, k, l) = kvec_list[i]
                 sin = 0
                 while sin <= 1:
                      kvec_list.append((h, k, l))
                      p = h * k1
                      q = k * k2
                      r = l * k3
                      L = p + q + r
                      K = (L[0]**2 + L[1]**2 + L[2]**2)**(0.5)
                      sin = lamb*(K/(4*math.pi))
                      l = l + 1
                  
            del kvec_list[0:n+1]

            n = len(kvec_list)
            print n

# aff and STRUCTURE FACTORS

            position = genfromtxt("SYM/output_file"+str(time)+"_time.xyz") #plik z pozycjami atomow WEJsCIOWY!!!!!!!!
            form = genfromtxt("AFF/AFF.txt")   #plik z AFF

            x1 = self.x1.get()
            x2 = self.x2.get()
            x3 = self.x3.get()
            print x1, x2, x3

            L1 = form[x1, 1:]
            L2 = form[x2, 1:]
            L3 = form[x3, 1:]

            inten_profile = []
            inten_profile0 = []
            inten_list = []
            theta_list = []
            sFactor_list = []
            dspace_list = []

            for i in range(n):
                 (h, k, l) = kvec_list[i]
                 p = h * k1
                 q = k * k2
                 r = l * k3
                 L = p + q + r
                 K = (L[0]**2 + L[1]**2 + L[2]**2)**(0.5)
     
                 y = K/(4*math.pi) 

                 f1 = L1[0] * exp(-L1[1]*y**2) + L1[2] * exp(-L1[3]*y**2) + L1[4] * exp(-L1[5]*y**2) + L1[6] * exp(-L1[7]*y**2) + L1[8]
                 f2 = L2[0] * exp(-L2[1]*y**2) + L2[2] * exp(-L2[3]*y**2) + L2[4] * exp(-L2[5]*y**2) + L2[6] * exp(-L2[7]*y**2) + L2[8]
                 f3 = L3[0] * exp(-L3[1]*y**2) + L3[2] * exp(-L3[3]*y**2) + L3[4] * exp(-L3[5]*y**2) + L3[6] * exp(-L3[7]*y**2) + L3[8]
             
                 for m in range(0,At1):
                      P = position[m,:]
                      Q = [h, k, l]
                      sFactor = f1*exp(2*math.pi*(P[1]*Q[0] + P[2]*Q[1] + P[3]*Q[2])*1j)
                      sFactor_list.append(sFactor)
                      m = m + 1
          
                 for t in range(At1, At1At2):
                      P = position[t,:]
                      Q = [h, k, l]
                      sFactor = f2*exp(2*math.pi*(P[1]*Q[0] + P[2]*Q[1] + P[3]*Q[2])*1j)
                      sFactor_list.append(sFactor)
                      t = t + 1
                 for u in range(At1At2, At1At2At3):
                      P = position[u,:]
                      Q = [h, k, l]
                      sFactor = f3*exp(2*math.pi*(P[1]*Q[0] + P[2]*Q[1] + P[3]*Q[2])*1j)
                      sFactor_list.append(sFactor)
                      u = u + 1
                 inten = abs(sum(sFactor_list))
                    
                 inten_list.append(inten)
     
                 zas = lamb*(y) 
     
                 if zas < 1:

                      theta0 = math.asin(zas)
          
                      theta = theta0 * (2*180/math.pi)
                      
                 theta_list.append(theta)

                 inten_profile0.append((h, k, l, theta, inten, inten**2))
                 inten_profile.append((round(theta,1), round(inten**2,3)))
                 
                 del sFactor_list[:]
                 i = i + 1

            A = array(inten_profile)
                        
            print time
            
            savetxt('SYMx/WIDMO_'+str(time)+'_time.xy', A)
            B = array(inten_profile0)
            savetxt('SYMx0/WIDMO_0_'+str(time)+'_time.xy', B)


#############################################################################
#################EXPERIMENTAL GRAPH##########################################
#############################################################################


    def expGraph(self):
        global CompareFilePath
        global ExpFilePath

        fig = plt.figure()
        p = fig.add_subplot(1,1,1)
        b = fig.add_subplot(1,1,1)
        
        xe = []
        ye = []

        xw = []
        yw = []
        
        graph_data = open(ExpFilePath).read()
        graph_data_1 = open(CompareFilePath).read()
        
        print ExpFilePath
        print CompareFilePath
        lines = graph_data.split('\n')
        liness = graph_data_1.split('\n')

        width = 0.01
        
        for line in lines:
            if len(line)>1:
                x, y = line.split()
                xe.append(float(x))
                ye.append(float(y))
        line = 0
        x=0
        y=0
        for line in liness:
            if len(line)>1:
                x, y = line.split()
                xw.append(float(x))
                yw.append(float(y))
        line = 0
          
        b = plt.bar(xw, yw, width)
        p = plot(xe, ye, color='r', lw = 2.0)          
        plt.ylabel('Intensity')
        plt.xlabel('2 theta')
        plt.title('Experimental Pattern')
   
        plt.show()
#########################################################################
############EXIT PROGRAM#################################################
#########################################################################
        
    def close(self): 
        root.destroy()

#########################################################################
###########SET ZERO######################################################
#########################################################################

    def setzero(self):
        global ExpFilePath

        x1 = []      
        y1 = []
        y2 = []
        N = 0        
                       
        graph_data = open(ExpFilePath).read()
        lines = graph_data.split('\n')

        for line in lines:
            if len(line)>1:
                x,y = line.split()
                x1.append(float(x))
                y1.append(float(y))
                N=N+1

        m = y1[0]
        f = open(ExpFilePath + 'Set_0_.xy' , 'w+')
        for i in y1:
            if i < m:
                m=i
        for i in y1:
            i = i - m
            y2.append(i) 

        for j in range(1, N):
            
            f.write(str(x1[j]))
            f.write(' ')
            f.write(str(y2[j]))
            f.write("\n")

        f.close()
        
#############################################################################
###########INTENS MATCH######################################################
#############################################################################
        
    def intenMatch(self):
        dt = self.dt.get()
        maxtime = self.maxtime.get()
        time = 0

        global ExpFilePath

        x1 = []      
        y1 = []
        yNew1 = []
        
        new_max = 1
        new_min = 0
        N = 0        
                       
        graph_data = open(ExpFilePath).read()
        lines = graph_data.split('\n')

        for line in lines:
            if len(line)>1:
                x,y = line.split()
                x1.append(float(x))
                y1.append(float(y))
                N=N+1

        minimum = y1[0]
        maximum = y1[0]

        f = open(ExpFilePath + '_Normalized_.xy' , 'w+')
                    
        for i in y1:
            if i < minimum:
                minimum = i
        for i in y1:
            if i > maximum:
                maximum = i

        for i in y1:
            i = (i - minimum)/(maximum - minimum)*(new_max-new_min) + new_min

            yNew1.append(i) 

        for j in range(1, N):
            
            f.write(str(x1[j]))
            f.write(' ')
            f.write(str(yNew1[j]))
            f.write("\n")

        f.close()
        

        
        while time <= maxtime:
            x2 = []
            y2 = []
            yNew2 = []
            i=0
            j=0
            N=0
            maximum = 0
            minimum = 0
            x=0
            y=0
            
            print time

            graph_data1 = open("SYMx/WIDMO_" + str(time) + "_time.xy", 'r+').read()
            lines1 = graph_data1.split('\n')
            #print "SYMx\WIDMO_" + str(time) + "_time.txt"
            for line in lines1:
                if len(line)>1:
                    x,y = line.split(' ')
                    x2.append(float(x))
                    y2.append(float(y))
                    N=N+1
                        
            minimum = y2[0]
            maximum = y2[0]

            f2 = open("SYMx_Norm\WIDMO_" + str(time) + "_time_Normalized.xy", 'w+')
                   
            for i in y2:
                if i < minimum:
                    minimum = i
            for i in y2:
                if i > maximum:
                    maximum = i

            for i in y2:
                i = (i - minimum)/(maximum - minimum)*(new_max-new_min) + new_min

                yNew2.append(i) 

            for j in range(1, N):
            
                f2.write(str(x2[j]))
                f2.write(' ')
                f2.write(str(yNew2[j]))
                f2.write("\n")

            f2.close()
            time = time + dt
            del x2, y2, yNew2
  
#####################################################################################       
# MAIN BLOCK#########################################################################
#####################################################################################
        
root = Tk()

app = App(root)

root.mainloop()

#root.destroy()

