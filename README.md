# DISTorX v1.0 #

Scientific application to simulation of distortion on complex crystaline structures and its impatc on the XRD patterns.

This repository contains source code of **DISTorX v1.0** program written in *Python 2.7* with *Vpython* module and additional sample files. 

Program have usage in simulation of distortion caused by Charge-Density-Wave directly affecting XRD patterns.

## Instructions how to set up program ##

### Windows Users ###

Install *Python 2.7* ([python.org](Link URL)) and *Vpython* ([vpython.org](Link URL)).

Program has been tested on Windows 7, 8.1, 10 and on different computers. 
After instalation of *Python* you should install required for the operation of the program libraries (*pylab, matplotlib, numpy, tkinter*).
To install libraries on Windows run CMD and use: 
```
#!cmd

pip install <packagename>
```
If "pip" is not recognaized, entry full path to script for example: 
```
#!cmd

C:/Python27/Scripts/pip install matplotlib
```
Python "Idle" - interpreter will inform you wich additional libraries do you need to run program.
In Windows you can run program by *cmd*, directry clicking on *DISTorX.py* icon, or by *python idle*.
First run takes more time.

### Linux Users ###

Program was also tested on *Linux Mint 17.1 'Rebeca'* distribution. Istallation of *Vpython* in this case was by Software manager (graphical linux windowed manager). On *Linux* there are couple of things you should do manually. Install *Python Idle* by: 
```
#!bash

$ apt-get install idle
```
Idle helps with identification of libraries and its nescesery to operate the program. Installation on *linux* is more difficult due to the manual installation of libraries. To do that you need to use 
```
#!bash

$ pip install <library name>
```
, and also 
```
#!bash

$ apt-get install <library name>
```
I encountered a few problems during the installation of *matplotlib* and after installation of this package reboot is needed (probably depend on *linux* distribution). One more, *linux-python-idle* did not recognize:
```
#!python

from matplotlib import style
```
Comment this line by #. Excuding *'import style'* does not affect the operation of the program. In *linux* terminal program work only when typing 
```
#!bash

$ python DISTorX\ v1.0.py
```
, when you try to run program directly in terminal 
```
#!bash

$ ./DISTorX\ v1.0.py
```
it doesent work, probably becouse of linux kernel configuration.

### Instructions ###

1. 	Run program. 

2. 	Select input file *.xyz, with atoms coordinates and elements labels.
	Program needs the number of line where element is labeled to obtain AFF (Atomic Form Factors).  (AFF.txt file)

3.	Set simulation parameters. **A** - an aplitude of distortion, **maxTime** - maximum time of simulation, 
	**dt** - time step, **a, b, c** - lattice parameters, **lambda** - x-ray wavelength.

4. 	After initiaze of input structure program can show visualization of structure in time dependency. (*Vpython*)
	
5. 	Program can generate XRD patterns with distortion or without:

	a) To simulate XRD pattern without distortion just click Simulate XRD. (this gives only one simulation pattern)
	
	b) To simulate distortion first run distortion simulation and then dynamic XRD simulation. Time of simulation depends on computer hardware. (this gives set of simulations patterns)

6.	Now you can manage results dirctly from *ASCII* files or use *DISTorX* to run Dynamic Plot (changes of XRD pattern in time dependency) or compare simulation with experimental results. 

7. 	Program have helping in comparision of simulated and experimental XRD patterns, modules:

	a) Intensity Match (normalizing intensities of experimental end simulation patterns to 1) 
	
	b) Set 0 (seting starting level for experimental intensities as 0).
	
Generated files are all saved in folders. Each new simulation override existing files. 

Feel free to testing, at the begining try samples (*TETRA.xyz*) and experiment sample. Be careful with the input files, program treats input structures as P1 symmetry.

###  ###

Lech Kalinowski
University of Silesia
Katowice, Poland

Repo owner and admin